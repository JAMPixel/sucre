#ifndef __SHOP__
#define __SHOP__

#include "structure.h"
#include "interface.h"

void init_shop(Element * personnages);
void achat_seed(Element * bouton_achat,int i);
void leave(Element * bouton_sortie, int i);


#endif
