#ifndef __TENTACLE__
#define __TENTACLE__

#include "structure.h"
#include "interface.h"

void init_tentacles(Element * tentacules[],Element * personnage);
void montee(Element * tentacules);
void attaquer_tentacule(Element * tentacules, int i);
void tentacle_rape(Element * tentacules);

#endif
