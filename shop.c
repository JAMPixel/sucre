#include "shop.h"


void init_shop(Element * personnages){

  int red[] = {255,0,0,255};
  Element * bouton_achat, * bouton_sortie;

  createImage(largeur_item,largeur_infos,W_Window-largeur_item,H_Window-largeur_infos,"./sprites/site.png",SHOP,20);

  bouton_achat = createImage(478,270,406,406,"./sprites/bouton_achat.png",SHOP,19);
  addElementToElement(bouton_achat,personnages);
  addClickableElement(bouton_achat,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(bouton_achat,achat_seed);

  bouton_sortie = createBlock(W_Window-20,largeur_infos,20,20,red,SHOP,19);
  addElementToElement(bouton_sortie,personnages);
  addClickableElement(bouton_sortie,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(bouton_sortie,leave);

}

void achat_seed(Element * bouton_achat,int i){
  int prix = 5;
  Element * personnages;
  perso_t * struct_perso;

  initIteratorElement(bouton_achat);
  personnages = nextIteratorElement(bouton_achat);
  getDataElement(personnages,(void **)&struct_perso);

  if(struct_perso->argent>=prix){

    majGraines(personnages,+1,SHOP);
    majArgent(personnages,-prix,SHOP);

  }
  (void)i;
}


void leave(Element * bouton_sortie, int i){
  Element * personnages;

  initIteratorElement(bouton_sortie);
  personnages = nextIteratorElement(bouton_sortie);

  setDisplayCodeWindow(MAISON);
  clearPlanDisplayCode(SHOP,0);
  clearPlanDisplayCode(SHOP,1);
  Affichage_interface(personnages,MAISON);
  majVie(personnages,0,MAISON);
  majEnergie(personnages,0,MAISON);
  majArgent(personnages,0,MAISON);

  (void)i;
}
