#include "champs.h"


/* Initialisation */
int black[] = {0,0,0,255};
int red[] = {255,0,0,255};
int white[] = {255,255,255,255};

void init_crop(Element * crop){
  crop_t * d_crop = (crop_t *) malloc(sizeof(crop_t));
  //crop->sprites
  d_crop->pdv = PDV_CROP1;
  d_crop->pdv_max = PDV_CROP1;
  d_crop->pdv_adulte = TPS_CROP1;
  d_crop->chance_atk = CHANCE_ATK;
  d_crop->attaque = 0;
  d_crop->tentacule = NULL;
  d_crop->occupe = 1; //test
  d_crop->debloque = 1; // TEST
  d_crop->frames=0;

  setDataElement(crop, (void *) d_crop);

  addClickableElement(crop,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(crop, interact_crop);
}

void init_champ(Element * champ[W_CHAMP][H_CHAMP], Element * perso){
  int i, j;
  int couleur[] = {255,0,0,255};
  for (i=0;i<W_CHAMP;i++){
    for (j=0;j<H_CHAMP;j++){
      champ[i][j] = createImage(X_CHAMP + i*T_CASE, Y_CHAMP + j*T_CASE, T_CASE, T_CASE, "./sprites/cristals.png", CHAMPS, 4);
      init_crop(champ[i][j]);
      couleur[0]-= 10;

      // chaînage du perso aux crops
      addElementToElement(champ[i][j], perso);
      setActionElement(champ[i][j],croissance);

      addAnimationElement(champ[i][j],0);
      addSpriteAnimationElement(champ[i][j],0,0,0,50,50,5,0);
      addSpriteAnimationElement(champ[i][j],0,50,0,50,50,5,1);
      addSpriteAnimationElement(champ[i][j],0,100,0,50,50,5,2);
      addSpriteAnimationElement(champ[i][j],0,150,0,50,50,5,3);
      setWaySpriteAnimationElement(champ[i][j],0,0);

      addAnimationElement(champ[i][j],1);
      addSpriteAnimationElement(champ[i][j],1,200,0,50,50,5,0);
      setWaySpriteAnimationElement(champ[i][j],1,0);

      addAnimationElement(champ[i][j],2);
      addSpriteAnimationElement(champ[i][j],2,250,0,50,50,5,0);
      setWaySpriteAnimationElement(champ[i][j],2,0);
    }
    couleur[1]+=10;
  }
}

/* Planter les cristaux : clickable*/
void interact_crop(Element * crop, int i){
  float x, y;
  crop_t * d_crop;
  perso_t * d_perso;

  initIteratorElement(crop);
  Element * perso = nextIteratorElement(crop);

  //perso chaîné aux crops
  getDataElement(crop, (void **) &d_crop);
  getDataElement(perso, (void **) &d_perso);
  //printf("clic gauche\n");

  getCoordElement(crop,&x,&y);

  switch (d_perso->objet_equipe){
    case PAXEL : //Planter ou récupérer
      //printf("%d, %d\n", d_crop->debloque , !d_crop->occupe);

      if (d_crop->debloque && !d_crop->occupe){ //si on peut planter

        if (i==1 &&   d_perso->sac->seeds[0]>0){

          init_crop(crop);
          majGraines(perso,-1,CHAMPS);
          setAnimationElement(crop,0);
          setSpriteAnimationElement(crop,0);

        }

        /*printf("On peut planter\n");
        Element * menu = createBlock(x+T_CASE, y, T_CASE, T_CASE, black, CHAMPS, 0);

        sprintf(temp,"%d",d_perso->sac->seeds[0]);
        strcat(text,temp);

        Element * choix = createText(x+T_CASE, y, T_CASE, T_CASE/3, 100, "arial.ttf", text, white, SANDAL2_BLENDED, CHAMPS, 0);
        Element * oui = createButton(x+T_CASE, y+T_CASE/3, T_CASE, T_CASE/3, 100, "arial.ttf", "oui", white, SANDAL2_BLENDED, black, CHAMPS, 0);
        Element * non = createButton(x+T_CASE, y+2*T_CASE/3, T_CASE, T_CASE/3, 100, "arial.ttf", "non", white, SANDAL2_BLENDED, black, CHAMPS, 0);

        setDataElement(oui, (void *) crop);

        // Choix
        addClickableElement(oui,rectangleClickable(0.f,0.f,1.f,1.f),0);
        if (d_perso->sac->seeds[0]<=0){
          setOnClickElement(oui,delElement);
          createText(x, y, T_CASE, T_CASE, 100  , "arial.ttf", "No can do", white, SANDAL2_BLENDED, CHAMPS, 3);
          //SDL_Delay(500);
        }
        else{
          setOnClickElement(oui, planter);
        }
        addClickableElement(non,rectangleClickable(0.f,0.f,1.f,1.f),0);
        setOnClickElement(non,delElement);*/
      }else{
        //printf("%d %d %d\n",d_crop->occupe, d_crop->pdv_max, d_crop->pdv_adulte );
        if (d_crop->occupe && d_crop->pdv_max >= d_crop->pdv_adulte){ //on peut harvest
          d_crop->occupe = 0;
          setAnimationElement(crop,1);
          majArgent(perso, 10, CHAMPS);
        }
      }
      //d_perso->sac->seeds[]
    break;
    case EPEE :
      if (d_crop->attaque){ //présence de tentacule

        printf("tentacule attaque\n");
        tentacule_t * d_tentacule;
        getDataElement(d_crop->tentacule, (void **) &d_tentacule);
        d_tentacule->pdv-=5;
        if (d_tentacule->pdv<=0){
          setAnimationElement(crop,0);
          printf("mort du tentacule\n");
          int violet[] = {255,0,255,255};
          setColorElement(crop, violet);
          d_tentacule->pdv = 10;
          d_tentacule->pdv_max = 10;
          d_tentacule->hauteur_caverne = 0;
          d_tentacule->frames = 0;
          d_tentacule->deplacement = -1;
          d_tentacule->etage = 3;
          d_tentacule->crop_cible = NULL;

          moveElement(d_crop->tentacule, 0, H_Window-largeur_infos);
        }
      }
    break;
    default :
      printf("Pas paxel\n");


  } /**/
}


/* Attaque tentacules */
int atk_tentacule(Element * tentacule, Element * champ[W_CHAMP][H_CHAMP]){
  srandom(time(0));
  tentacule_t * data_tent;
  getDataElement(tentacule, (void **) &data_tent);

  if (data_tent->frames>=200 && data_tent->hauteur_caverne >= H_Window - largeur_infos){ // le tentacule a atteint la surface
    if (data_tent->crop_cible){ //tentacule en train d'attaquer
      Element * crop = data_tent->crop_cible;
      crop_t * data_crop;
      getDataElement(crop, (void **) &data_crop);
      data_crop->pdv-= TENT_ATK;
      if (data_crop->pdv<0){ // le crop décède tragiquement / TENTACULE ENTRAINE CROP DANS CAVERNE
        printf("décès\n");
        data_crop->occupe = 0;

        // Sprite changé à vide
        setAnimationElement(data_tent->crop_cible,1);

        int jaune[] = {255,255,0,255};
        setColorElement(crop, jaune);
        data_tent->crop_cible = NULL;
        data_tent->deplacement = +1; //Descend
      }
    }
    else{ // le tentacule est en attente
      int x = W_CHAMP * ((int)rand()/(float)RAND_MAX);
      int y = H_CHAMP * ((int)rand()/(float)RAND_MAX);
      Element * cible = champ[x][y];
      if (cible){
        crop_t * data_crop;
        getDataElement(cible, (void **) &data_crop);
        float attaque =  rand()/(float)RAND_MAX; //booléen d'attaque

        if ((attaque > (data_tent->chance_atk)/100.0) && data_crop->occupe && !data_crop->attaque){ // le tentacule attaque le cristal
          // Entre en phase d'attaque
          //Sprite
          printf("changements\n");
          setAnimationElement(data_tent->crop_cible,2);

          //int bleu[] = {0,0,255,255};
          //setColorElement(cible, bleu);
          data_crop->attaque = 1;
          data_crop->tentacule = tentacule;
          data_tent->crop_cible = cible;
        }
      }
    }
  data_tent->frames=0;
  }
  data_tent->frames++;

  return 0;
}

/* Croissance des cristaux */
void croissance(Element * crop){
  crop_t * data;
  int taux;

  getDataElement(crop, (void **) &data);

  //printf("%d %d %d %d\n",data->pdv,data->pdv_max,data->pdv_adulte,data->frames);
  //printf("%d\n",data->frames );

  if (data->pdv>0 && data->pdv_max< data->pdv_adulte && data->frames==200){

    data->pdv_max=data->pdv_max+1;
    data->pdv=data->pdv+1;
    data->frames=0;
    taux = (float)(data->pdv_max)/(data->pdv_adulte-data->pdv_max)*100;

    //printf("%d\n",taux);

    if(taux>=33 && taux <= 66){

      setSpriteAnimationElement(crop,1);

    }else if(taux>=66 && taux<100){

      setSpriteAnimationElement(crop,2);

    }else if(taux==100){

      setSpriteAnimationElement(crop,3);

    }
  }
  data->frames++;
  // changer le sprite selon la croissance
}
