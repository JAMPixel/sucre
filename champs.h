#ifndef __CHAMPS
#define __CHAMPS__

#include "structure.h"
#include "interface.h"

#define X_CHAMP 700
#define Y_CHAMP 200
#define W_CHAMP 6
#define H_CHAMP 6// en nb de cases
#define T_CASE 75

#define CHANCE_ATK 100
#define TENT_ATK 10

#define PDV_CROP1 10
#define TPS_CROP1 40

void init_champ(Element * champs[W_CHAMP][H_CHAMP], Element * perso);
int atk_tentacule(Element * tentacule, Element * champ[W_CHAMP][H_CHAMP]);
void interact_crop(Element * crop, int i);
void croissance(Element * crop);

#endif
