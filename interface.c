#include "interface.h"

void Affichage_interface(Element * personnage, int display){

  int white[] = {255,255,255,255};
  int red[] = {255,0,0,255};
  int green[] = {0,255,0,255};
  perso_t * struct_perso;
  Element * temp;
  Element * fenetre;
  char sprites[3][50]={"./sprites/arme.png","./sprites/outils.png","./sprites/outils.png"};

  initIteratorElement(personnage);
  fenetre = nextIteratorElement(personnage);

  getDataElement(personnage,(void **)&struct_perso);

  //création du fond
  createBlock(0,0,W_Window,largeur_infos,white,display,1);
  createBlock(0,0,largeur_item,H_Window,white,display,1);

  //creation items

  struct_perso->sac->items[struct_perso->objet_equipe] = createBlock(10,largeur_infos+10+struct_perso->objet_equipe*(largeur_item-10),largeur_item-20,largeur_item-20,green,display,0);

  for(int i=0;i<2;i++){
    if(i!=struct_perso->objet_equipe){

      struct_perso->sac->items[i] = createBlock(10,largeur_infos+10+i*(largeur_item-10),largeur_item-20,largeur_item-20,red,display,0);

    }

    createImage(15,largeur_infos+15+i*(largeur_item-10),largeur_item-30,largeur_item-30,sprites[i],display,0);

  }

  for(int i=0;i<4;i++){

    struct_perso->information[i] = createText(largeur_item+10+210*i,10,200,largeur_infos-20,100,"arial.ttf","-infini",green,SANDAL2_BLENDED,display,0);
    temp = createBlock(largeur_item+10+210*i,10,200,largeur_infos-20,red,display,1);
    addElementToElement(struct_perso->information[i],temp);

  }

  majVie(personnage,0,display);
  majEnergie(personnage,0,display);
  majArgent(personnage,0,display);
  majGraines(personnage,0,display);

  addClickableElement(fenetre,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(fenetre,changement_item);

}

void changement_item(Element * fenetre, int i){

    perso_t * struct_perso;
    Element * personnage;

    int green[] = {0,255,0,255};
    int red[] = {255,0,0,255};

    //printf("%d\n",i);

    if(i==2){

      initIteratorElement(fenetre);
      personnage = nextIteratorElement(fenetre);
      getDataElement(personnage,(void **)&struct_perso);
      setColorElement(struct_perso->sac->items[struct_perso->objet_equipe],red);
      struct_perso->objet_equipe = (struct_perso->objet_equipe+1)%2;
      setColorElement(struct_perso->sac->items[struct_perso->objet_equipe],green);
      //printf("Click %d\n",struct_perso->objet_equipe);

    }
}

void majVie(Element * personnage, int changements, int display){

  int blue[] = {0,0,255,255};
  perso_t * struct_perso;
  char text[10];
  int pc = 0;
  float x,y,w,h;

  getDataElement(personnage,(void **)&struct_perso);
  struct_perso->pdv = struct_perso->pdv + changements;
  getCoordElement(struct_perso->information[0],&x,&y);
  getDimensionElement(struct_perso->information[0],&w,&h);

  if(struct_perso->pdv>struct_perso->pdv_max){

      struct_perso->pdv=struct_perso->pdv_max;

  }

  pc = (float)struct_perso->pdv / (float)struct_perso->pdv_max * 100;

  sprintf(text,"%d",pc);
  strcat(text,"%");
  delElement(struct_perso->information[0]);
  struct_perso->information[0]=createText(x,y,w,h,100,"arial.ttf",text,blue,SANDAL2_BLENDED,display,0);


}

void majEnergie(Element * personnage, int changements, int display){

  int blue[] = {0,0,255,255};
  perso_t * struct_perso;
  char text[10];
  int pc = 0;
  float x,y,w,h;

  getDataElement(personnage,(void **)&struct_perso);
  struct_perso->energie = struct_perso->energie + changements;
  getCoordElement(struct_perso->information[1],&x,&y);
  getDimensionElement(struct_perso->information[1],&w,&h);

  if(struct_perso->energie>struct_perso->energie_max){

      struct_perso->energie=struct_perso->energie_max;

  }

  pc = (float)struct_perso->energie / (float)struct_perso->energie_max * 100;

  sprintf(text,"%d",pc);
  strcat(text,"%");
  delElement(struct_perso->information[1]);
  struct_perso->information[1]=createText(x,y,w,h,100,"arial.ttf",text,blue,SANDAL2_BLENDED,display,0);


}

void majArgent(Element * personnage, int changements, int display){

  int blue[] = {0,0,255,255};
  perso_t * struct_perso;
  char text[10];
  float x,y,w,h;

  getDataElement(personnage,(void **)&struct_perso);
  struct_perso->argent = struct_perso->argent + changements;
  getCoordElement(struct_perso->information[2],&x,&y);
  getDimensionElement(struct_perso->information[2],&w,&h);

  sprintf(text,"%d",struct_perso->argent);
  strcat(text,"$");
  delElement(struct_perso->information[2]);
  struct_perso->information[2]=createText(x,y,w,h,100,"arial.ttf",text,blue,SANDAL2_BLENDED,display,0);


}

void majGraines(Element * personnage, int changements, int display){

  int blue[] = {0,0,255,255};
  perso_t * struct_perso;
  char text[10];
  float x,y,w,h;

  getDataElement(personnage,(void **)&struct_perso);
  struct_perso->sac->seeds[0] = struct_perso->sac->seeds[0] + changements;
  getCoordElement(struct_perso->information[3],&x,&y);
  getDimensionElement(struct_perso->information[3],&w,&h);

  sprintf(text,"%d",struct_perso->sac->seeds[0]);
  strcat(text,"Gr");
  delElement(struct_perso->information[3]);
  struct_perso->information[3]=createText(x,y,w,h,100,"arial.ttf",text,blue,SANDAL2_BLENDED,display,0);


}

void porte(Element * personnages[], int x, int y,int w, int h, int depart, int * arrive, char * sprite){

  int black[] = {0,255,0,255};
  Element * tp_porte = createImage(x,y,w,h,sprite,depart,10);
  warp_t * struct_porte = (warp_t *)malloc(sizeof(warp_t));

  struct_porte->personnages[0]=personnages[0];
  struct_porte->personnages[1]=personnages[1];
  struct_porte->personnages[2]=personnages[2];
  struct_porte->depart=depart;
  struct_porte->arrive=*arrive;
  setDataElement(tp_porte,(void *)struct_porte);

  addElementToElement(tp_porte,personnages[depart]);
  setActionElement(tp_porte,tp);
}

void tp(Element * tp_porte){

  Element * personnage;
  warp_t * struct_porte;
  perso_t * struct_perso;
  float x_porte, y_porte, w_porte, h_porte;
  float x_perso, y_perso, w_perso, h_perso;

  getDataElement(tp_porte,(void **)&struct_porte);

  getCoordElement(tp_porte,&x_porte,&y_porte);
  getDimensionElement(tp_porte,&w_porte,&h_porte);

  initIteratorElement(tp_porte);
  personnage = nextIteratorElement(tp_porte);

  getDataElement(personnage,(void **)&struct_perso);

  getCoordElement(personnage,&x_perso,&y_perso);
  getDimensionElement(personnage,&w_perso,&h_perso);

  //printf("%f >= %f &&  %f <= %f && %f <= %f\n",x_perso+w_perso,x_porte,x_perso,x_porte+w_porte,y_perso+h_perso,y_porte+h_porte);

  if(x_perso+w_perso>=x_porte && x_perso<=x_porte+w_porte && y_perso+h_perso<=y_porte+h_porte && y_perso+h_perso>=y_porte){

    setDisplayCodeWindow(struct_porte->arrive);
    clearPlanDisplayCode(struct_porte->depart,0);
    clearPlanDisplayCode(struct_porte->depart,1);
    Affichage_interface(struct_porte->personnages[struct_porte->arrive],struct_porte->arrive);
    majVie(personnage,0,struct_porte->arrive);
    majEnergie(personnage,0,struct_porte->arrive);
    majArgent(personnage,0,struct_porte->arrive);

    struct_perso->display=struct_porte->arrive;

    if(struct_porte->arrive==CHAMPS){

      if(struct_porte->depart==MAISON){

        moveElement(struct_porte->personnages[CHAMPS],0,10);

      }else{

        moveElement(struct_porte->personnages[CHAMPS],0,-10);

      }


    }else if(struct_porte->arrive==MAISON){

      moveElement(struct_porte->personnages[MAISON],0,-10);

    }else if(struct_porte->arrive==CAVERNE){

      moveElement(struct_porte->personnages[CAVERNE],0,10);

    }
    //printf("%d\n",*arrive);
  }


}

void init_caverne(){

  //echelle
  createImage(largeur_item,largeur_infos,largeur_echelle,H_Window-largeur_infos,"./sprites/echelle.png",CAVERNE,4);

  //plateformes
  for(int i=0;i<4;i++){

    createImage(largeur_item+largeur_echelle,largeur_infos+i*(H_Window-largeur_infos-30)/3,W_Window-largeur_item-largeur_echelle,15,"./sprites/plateform_rock.png",CAVERNE,3);

  }

}
