#include "personnage.h"
#include "interface.h"

void Init_perso(Element * personnages[]){

  //int white[] = {255,255,255,255};
  //int gray[] = {100,100,100,255};
  //int black[] = {0,0,0,255};
  char sprites[3][50]={"./sprites/jardin.jpg","./sprites/plancher.jpg","./sprites/grotte.jpg"};

  Element * fenetre[3];
  personnages[CHAMPS] = createImage(600,600,W_perso,H_perso,"./sprites/3quarts.ss.png",CHAMPS,3);
  personnages[MAISON] = createImage(120,H_Window-20-H_perso,W_perso,H_perso,"./sprites/3quarts.ss.png",MAISON,3);
  personnages[CAVERNE] = createImage(120,70,W_perso-25,H_perso,"./sprites/joueur_profil.ss.png",CAVERNE,3);
  perso_t * struct_perso = (perso_t *)malloc(sizeof(perso_t));

  struct_perso->pdv = 100;
  struct_perso->pdv_max = 100;
  struct_perso->energie = 100;
  struct_perso->energie_max = 100;
  struct_perso->argent = 100;
  struct_perso->objet_equipe = 0;
  struct_perso->sac = (inventory_t *)malloc(sizeof(inventory_t));
  struct_perso->z = 0;
  struct_perso->q = 0;
  struct_perso->s = 0;
  struct_perso->d = 0;
  struct_perso->display = CHAMPS;

  for(int i=0;i<3;i++){

    struct_perso->information[i] = NULL;

  }

  struct_perso->sac->lvl_paxel=1;
  struct_perso->sac->lvl_epee=1;
  struct_perso->sac->takoyaki=0;
  struct_perso->sac->seeds[0] = 0;
  struct_perso->sac->seeds[1] = 0;
  struct_perso->sac->seeds[2] = 0;

  for(int i=0;i<2;i++){

    struct_perso->sac->items[i] = NULL;

  }

  for(int j=0;j<3;j++){

    fenetre[j] = createImage(0,0,W_Window,H_Window,sprites[j],j,20);

    setDataElement(personnages[j],(void *)struct_perso);

    setKeyPressedElement(personnages[j],key_press_zqsd);
    setKeyReleasedElement(personnages[j],key_release_zqsd);
    setActionElement(personnages[j],mouvement_perso);

    addElementToElement(fenetre[j],personnages[j]);
    addElementToElement(personnages[j],fenetre[j]);

    setActionElement(fenetre[j],collisions);


  }
  Affichage_interface(personnages[CHAMPS],CHAMPS);
  majVie(personnages[CHAMPS],0,CHAMPS);
  majEnergie(personnages[CHAMPS],0,CHAMPS);
  majArgent(personnages[CHAMPS],0,CHAMPS);

  for(int j=0;j<2;j++){

    //ajout des animations

    //3/4

    for(int k=0;k<2;k++){

      addAnimationElement(personnages[j],k);
      addSpriteAnimationElement(personnages[j],k,0+150*k,0,50,50,5,0);
      addSpriteAnimationElement(personnages[j],k,50+150*k,0,50,50,5,1);
      addSpriteAnimationElement(personnages[j],k,0+150*k,0,50,50,5,2);
      addSpriteAnimationElement(personnages[j],k,100+150*k,0,50,50,5,3);
      setWaySpriteAnimationElement(personnages[j],k,0);

    }

    addAnimationElement(personnages[j],2);
    addSpriteAnimationElement(personnages[j],2,0+150*2,0,50,50,5,0);
    addSpriteAnimationElement(personnages[j],2,50+150*2,0,50,50,5,1);
    addSpriteAnimationElement(personnages[j],2,0+150*2,0,50,50,5,2);
    setWaySpriteAnimationElement(personnages[j],2,0);

    addAnimationElement(personnages[j],3);
    addSpriteAnimationElement(personnages[j],3,100+150*2,0,50,50,5,0);
    addSpriteAnimationElement(personnages[j],3,150+150*2,0,50,50,5,1);
    addSpriteAnimationElement(personnages[j],3,100+150*2,0,50,50,5,2);
    setWaySpriteAnimationElement(personnages[j],3,0);

  }

  //côté

  for(int k=0;k<2;k++){

    addAnimationElement(personnages[2],k);
    addSpriteAnimationElement(personnages[2],k,0+150*k,0,50,100,5,0);
    addSpriteAnimationElement(personnages[2],k,50+150*k,0,50,100,5,1);
    addSpriteAnimationElement(personnages[2],k,0+150*k,0,50,100,5,2);
    addSpriteAnimationElement(personnages[2],k,100+150*k,0,50,100,5,3);
    setWaySpriteAnimationElement(personnages[2],k,0);

  }

}

void key_press_zqsd(Element * this, SDL_Keycode c){
  perso_t * struct_perso;
  float x,y,w,h;
  int palier=0;
  getDataElement(this,(void**)&struct_perso);
  getCoordElement(this,&x,&y);
  getDimensionElement(this,&w,&h);

  if(struct_perso->display==CAVERNE){

    for(int i=1;i<4;i++){

      palier = palier || (y+h<=largeur_infos+i*(H_Window-largeur_infos-30)/3 && y+h>=largeur_infos+i*(H_Window-largeur_infos-30)/3-10);

    }

  }

  //printf("%d %d\n",palier,struct_perso->display==CAVERNE);

  switch (c) {
    //direction x
    case 'q':
    {
      if(struct_perso->display!=CAVERNE || palier){

        struct_perso->q=1;
        setAnimationElement(this,0);
        setWaySpriteAnimationElement(this,0,1);

      }
      break;
    }
    case 'd':
    {
      if(struct_perso->display!=CAVERNE || palier){

        struct_perso->d=1;
        setAnimationElement(this,1);
        setWaySpriteAnimationElement(this,1,1);

      }
      break;
    }
    //direction y
    case 'z':
    {
      if(struct_perso->display!=CAVERNE || (struct_perso->display==CAVERNE && x+w<=largeur_item+largeur_echelle)){

        struct_perso->z=1;
        if(struct_perso->display!=CAVERNE){

          setAnimationElement(this,2);
          setWaySpriteAnimationElement(this,2,1);

        }

      }
      break;
    }
    case 's':
    {
      if(struct_perso->display!=CAVERNE || (struct_perso->display==CAVERNE && x+w<=largeur_item+largeur_echelle)){

        struct_perso->s=1;
        if(struct_perso->display!=CAVERNE){

          setAnimationElement(this,3);
          setWaySpriteAnimationElement(this,3,1);

        }
      }
      break;
    }
  }
}

void key_release_zqsd(Element * this, SDL_Keycode c){

  perso_t * struct_perso;
  getDataElement(this,(void**)&struct_perso);

  switch (c) {
    //direction x
    case 'q':
    {
      struct_perso->q=0;
      setSpriteAnimationElement(this,0);
      setWaySpriteAnimationElement(this,0,0);
      break;
    }
    case 'd':
    {
      struct_perso->d=0;
      setSpriteAnimationElement(this,0);
      setWaySpriteAnimationElement(this,1,0);
      break;
    }
    //direction y
    case 'z':
    {
      struct_perso->z=0;
      setSpriteAnimationElement(this,0);
      setWaySpriteAnimationElement(this,2,0);
      break;
    }
    case 's':
    {
      struct_perso->s=0;
      setSpriteAnimationElement(this,0);
      setWaySpriteAnimationElement(this,3,0);
      break;
    }
  }
}

void mouvement_perso(Element * this){

  perso_t * struct_perso;
  getDataElement(this,(void**)&struct_perso);
  moveElement(this,5*(struct_perso->d-struct_perso->q),5*(struct_perso->s-struct_perso->z));

}

void collisions(Element * fenetre){

  Element * personnage;
  float x_perso,y_perso,w_perso,h_perso;
  perso_t * struct_perso;

  initIteratorElement(fenetre);

  personnage = nextIteratorElement(fenetre);

  getCoordElement(personnage,&x_perso,&y_perso);
  getDimensionElement(personnage,&w_perso,&h_perso);
  getDataElement(personnage,(void **)&struct_perso);

  if(x_perso<=largeur_item){
    struct_perso->q=0;
    moveElement(personnage,1,0);
  }else if(x_perso+w_perso>=W_Window){
    struct_perso->d=0;
    moveElement(personnage,-1,0);
  }

  if(y_perso+h_perso<=largeur_infos){
    struct_perso->z=0;
    moveElement(personnage,0,1);
  }else if(y_perso+h_perso>=H_Window){
    struct_perso->s=0;
    moveElement(personnage,0,-1);
  }

}
