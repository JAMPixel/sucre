#include "tentacle.h"

void init_tentacles(Element * tentacules[],Element * personnages){

  int green[] = {0,255,0,255};
  int hauteur;
  int ecart = (H_Window-largeur_infos)/3-40;
  tentacule_t * struct_tentacle[5];
  int hauteur_max = H_Window-largeur_infos;

  srand(time(0));

  for(int i=0;i<5;i++){

    hauteur = rand()%500-20;

    tentacules[i]=createImage(largeur_item+largeur_echelle+20+i*((W_Window-largeur_item-largeur_echelle)/4-20),H_Window-hauteur,20,hauteur_max+10,"./sprites/_sucre_tentacule.png",CAVERNE,3);

    struct_tentacle[i]=(tentacule_t *)malloc(sizeof(tentacule_t));
    struct_tentacle[i]->pdv = 10;
    struct_tentacle[i]->pdv_max = 10;
    struct_tentacle[i]->chance_atk = 40;
    struct_tentacle[i]->hauteur_caverne = hauteur;
    struct_tentacle[i]->etage = 3-hauteur/ecart;
    struct_tentacle[i]->crop_cible = NULL;
    struct_tentacle[i]->frames = 0;
    struct_tentacle[i]->deplacement = 1;
    struct_tentacle[i]->run=1;
    setDataElement(tentacules[i],(void *)struct_tentacle[i]);
    addClickableElement(tentacules[i],rectangleClickable(0.f,0.f,1.f,1.f),0);
    setOnClickElement(tentacules[i],attaquer_tentacule);
    addElementToElement(tentacules[i],personnages);
    setActionElement(tentacules[i],tentacle_rape);
    //printf("%d\n",struct_tentacle[i]->etage);
  }

}

void montee(Element * tentacules){
  int ecart = (H_Window-largeur_infos)/3-40;
  int hauteur_max = H_Window-largeur_infos;
  tentacule_t * struct_tentacle;
  getDataElement(tentacules,(void **)&struct_tentacle);


  if(struct_tentacle->frames==200  && struct_tentacle->hauteur_caverne<hauteur_max){

    moveElement(tentacules,0,-struct_tentacle->deplacement*5);
    struct_tentacle->hauteur_caverne = struct_tentacle->hauteur_caverne+struct_tentacle->deplacement*5;
    struct_tentacle->etage = 3-struct_tentacle->hauteur_caverne/ecart;
    struct_tentacle->frames = 0;

  }

  struct_tentacle->frames++;

}

void attaquer_tentacule(Element * tentacules, int i){
  Element * personnages;
  perso_t * struct_perso;
  tentacule_t * struct_tentacle;
  float x_perso, y_perso, w_perso, h_perso;
  float x_tenta, y_tenta, w_tenta, h_tenta;
  int hauteur_perso;
  int etage_joueur;
  int ecart = (H_Window-largeur_infos)/3-40;

  initIteratorElement(tentacules);
  personnages = nextIteratorElement(tentacules);
  getDataElement(personnages,(void **)&struct_perso);
  getDataElement(tentacules,(void **)&struct_tentacle);

  getCoordElement(tentacules,&x_tenta,&y_tenta);
  getDimensionElement(tentacules,&w_tenta,&h_tenta);

  getCoordElement(personnages,&x_perso,&y_perso);
  getDimensionElement(personnages,&w_perso,&h_perso);

  hauteur_perso = H_Window-y_perso;
  etage_joueur = 3-hauteur_perso/ecart;
  //printf("%d\n",etage_joueur );

  //printf("%d, %d == %d, %f >= %f, %f <= %f\n",struct_perso->objet_equipe,etage_joueur,struct_tentacle->etage,x_perso+w_perso,x_tenta-40,x_perso,x_tenta+w_tenta+40);

  if(struct_perso->objet_equipe==EPEE && etage_joueur==struct_tentacle->etage && x_perso+w_perso>=x_tenta-100 && x_perso<=x_tenta+w_tenta+100){
    if(struct_perso->energie>0){
      majEnergie(personnages,-5,CAVERNE);
      moveElement(tentacules,0,10);
      struct_tentacle->hauteur_caverne = struct_tentacle->hauteur_caverne-15;
      struct_tentacle->etage = 3-struct_tentacle->hauteur_caverne/ecart;
      struct_tentacle->frames=-200;
    }
  }

  (void)i;

}

void tentacle_rape(Element * tentacules){
  Element * personnages;
  perso_t * struct_perso;
  tentacule_t * struct_tentacle;
  float x_perso, y_perso, w_perso, h_perso;
  float x_tenta, y_tenta, w_tenta, h_tenta;
  int hauteur_perso,etage_joueur;
  int ecart = (H_Window-largeur_infos)/3-40;

  initIteratorElement(tentacules);
  personnages = nextIteratorElement(tentacules);
  getDataElement(personnages,(void **)&struct_perso);
  getDataElement(tentacules,(void **)&struct_tentacle);

  getCoordElement(tentacules,&x_tenta,&y_tenta);
  getDimensionElement(tentacules,&w_tenta,&h_tenta);

  getCoordElement(personnages,&x_perso,&y_perso);
  getDimensionElement(personnages,&w_perso,&h_perso);

  hauteur_perso = H_Window-y_perso;
  etage_joueur = 3-hauteur_perso/ecart;

  if(etage_joueur==struct_tentacle->etage && x_perso+w_perso>=x_tenta && x_perso<=x_tenta+w_tenta){
    moveElement(personnages,-w_tenta,0);
    majVie(personnages,-10,CAVERNE);
    if(struct_perso->pdv<=0){
      struct_tentacle->run=0;
    }
  }
}
