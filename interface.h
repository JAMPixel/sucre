#ifndef __INTERFACE__
#define __INTERFACE__

#include "structure.h"

void Affichage_interface(Element * personnage, int display);
void changement_item(Element * fenetre,int i);
void majVie(Element * personnage, int changements,int display);
void majEnergie(Element * personnage, int changements, int display);
void majArgent(Element * personnage, int changements, int display);
void majGraines(Element * personnage, int changements, int display);
void porte(Element * personnages[], int x, int y,int w, int h, int depart, int * arrive, char * sprite);
void tp(Element * tp_porte);
void init_caverne();

#endif
