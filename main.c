#include "structure.h"
#include "personnage.h"
#include "interface.h"
#include "champs.h"
#include "tentacle.h"
#include "maison.h"
#include "shop.h"


int main(){

  int black[] = {0,0,0,255};
  Element * champ[W_CHAMP][H_CHAMP];
  Element * personnages[3];
  Element * tentacules[5];
  int arrive,arrive1,arrive2,arrive3;
  int run=1;
  tentacule_t * struct_tentacle;

  if(initAllSANDAL2(IMG_INIT_PNG)){
    puts("Failed to init SANDAL2");
    return -1;
  }

  if(createWindow(W_Window,H_Window,"Fenêtre",0,black,0)){
    puts("Failed to open the window");
    closeAllSANDAL2();
  }

  Init_perso(personnages);
  init_caverne();
  init_tentacles(tentacules,personnages[CAVERNE]);
  init_maison(personnages[MAISON]);
  init_shop(personnages[MAISON]);
  init_champ(champ, personnages[CHAMPS]);

  arrive = MAISON;
  porte(personnages,120,70,100,40,CHAMPS,&arrive,"./sprites/_port_champ_ext.png");
  arrive1 = CHAMPS;
  porte(personnages,120,H_Window-10,100,30,MAISON,&arrive1,"./sprites/_doorstep_int.png");
  arrive2 = CAVERNE;
  porte(personnages,400,400,100,30,CHAMPS,&arrive2,"./sprites/_trou_caverne.png");
  arrive3 = CHAMPS;
  porte(personnages,120,70,100,30,CAVERNE,&arrive3,"./sprites/_doorstep_int.png");

  while(!PollEvent(NULL) && run){

    run=1;

    for(int k=0;k<5;k++){
      getDataElement(tentacules[k],(void **)&struct_tentacle);
      run=struct_tentacle->run && run;
      montee(tentacules[k]);
      atk_tentacule(tentacules[k], champ);
      //tentacle_rape(tentacules[k]);
    }

    updateWindow();
    displayWindow();
    SDL_Delay(16);

  }

  closeAllWindow();
  closeAllSANDAL2();


  return 0;
}
