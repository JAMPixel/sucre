#ifndef __PERSO__
#define __PERSO__

#include "structure.h"

void Init_perso(Element * personnages[]);

void key_press_zqsd(Element * this, SDL_Keycode c);

void key_release_zqsd(Element * this, SDL_Keycode c);

void mouvement_perso(Element * this);

void collisions(Element * fenetre);

#endif
