#ifndef __STRUCTURE__
#define __STRUCTURE__

#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

#define W_perso 100
#define H_perso 100

#define largeur_item 120
#define largeur_infos 70
#define largeur_echelle 100

#define H_Window  700
#define W_Window  1300

#define CHAMPS 0
#define MAISON 1
#define CAVERNE 2
#define SHOP 3

#define EPEE 0
#define PAXEL 1
#define TAKOYAKI 2

typedef struct inventory{
  int lvl_paxel;
  int lvl_epee;
  int takoyaki;
  int seeds[3];
  Element * items[3];
}inventory_t;

typedef struct perso{
  int pdv;
  int pdv_max;
  int energie;
  int energie_max;
  int argent;
  int objet_equipe;
  Element * information[4];
  int display;
  inventory_t * sac;
  int z;
  int q;
  int s;
  int d;
}perso_t;

typedef struct crop{
  char sprites[5][20];
  int pdv; //pv actuels (avec les dégats)
  int pdv_max; //full health, augmentent quand la plante pousse
  int pdv_adulte; //pv de la plante adulte
  int chance_atk;
  int attaque;
  Element * tentacule;
  int occupe;
  int debloque;
  int frames;
}crop_t;

typedef struct tentacule{
  char sprites[5][20];
  int pdv;
  int pdv_max;
  int chance_atk;
  int hauteur_caverne;
  int etage;
  Element * crop_cible;
  int frames;
  int run;
  int deplacement; //+1 si tentacule monte, -1 s'il descend
}tentacule_t;

typedef struct warp{
  Element * personnages[3];
  int depart;
  int arrive;
}warp_t;
#endif
