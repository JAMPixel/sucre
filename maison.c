#include "maison.h"

void init_maison(Element * personnages){

  Element * lit, * ordi;
  //int blue[] = {0,0,255,255};
  int largeur_lit = 200,longeur_lit = 300;

  lit = createImage(W_Window-longeur_lit,largeur_infos,longeur_lit,largeur_lit,"./sprites/lit.png",MAISON,4);

  addClickableElement(lit,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(lit,dodo);
  addElementToElement(lit,personnages);

  ordi = createImage(W_Window-2*longeur_lit,largeur_infos,longeur_lit,largeur_lit,"./sprites/bureau.png",MAISON,4);

  addClickableElement(ordi,rectangleClickable(0.f,0.f,1.f,1.f),0);
  setOnClickElement(ordi,achat);
  addElementToElement(ordi,personnages);

}

void achat(Element * ordi,int i){

  Element * personnages;
  perso_t * struct_perso;
  float x_perso, y_perso, w_perso, h_perso;
  float x_ordi, y_ordi, w_ordi, h_ordi;

  initIteratorElement(ordi);
  personnages = nextIteratorElement(ordi);
  getDataElement(personnages,(void **)&struct_perso);

  getCoordElement(ordi,&x_ordi,&y_ordi);
  getDimensionElement(ordi,&w_ordi,&h_ordi);

  getCoordElement(personnages,&x_perso,&y_perso);
  getDimensionElement(personnages,&w_perso,&h_perso);

  if(x_perso+w_perso>=x_ordi-100 && x_perso<=x_ordi+w_ordi+100 && y_perso+h_perso>=y_ordi-100 && y_perso<=y_ordi+h_ordi+100){
    setDisplayCodeWindow(SHOP);
    clearPlanDisplayCode(MAISON,0);
    clearPlanDisplayCode(MAISON,1);
    Affichage_interface(personnages,SHOP);
    majVie(personnages,0,SHOP);
    majEnergie(personnages,0,SHOP);
    majArgent(personnages,0,SHOP);
  }

  (void)i;
}

void dodo(Element * lit,int i){
  Element * personnages;
  perso_t * struct_perso;
  float x_perso, y_perso, w_perso, h_perso;
  float x_lit, y_lit, w_lit, h_lit;
  int manque_pdv, manque_energie;

  initIteratorElement(lit);
  personnages = nextIteratorElement(lit);
  getDataElement(personnages,(void **)&struct_perso);

  getCoordElement(lit,&x_lit,&y_lit);
  getDimensionElement(lit,&w_lit,&h_lit);

  getCoordElement(personnages,&x_perso,&y_perso);
  getDimensionElement(personnages,&w_perso,&h_perso);

  if(x_perso+w_perso>=x_lit && x_perso<=x_lit+w_lit && y_perso+h_perso>=y_lit && y_perso<=y_lit+h_lit){

    manque_pdv=struct_perso->pdv_max-struct_perso->pdv;
    manque_energie=struct_perso->energie_max-struct_perso->energie;
    majVie(personnages,manque_pdv,MAISON);
    majEnergie(personnages,manque_energie,MAISON);
  }

  (void)i;

}
